//
//  Property.swift
//  Engineer_iOS
//
//  Created by Macstorm on 20.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import Foundation
import CoreData

class Property{
    
    var dependency: [Dependency]?
    var name: String!
    var baseValue: Int!
    var propertyObject: NSManagedObject!
    
    init(name: String!, baseValue: Int!, propertyObject: NSManagedObject!){
        
        self.name = name
        self.baseValue = baseValue
        self.propertyObject = propertyObject
        
    }
}