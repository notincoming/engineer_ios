//
//  NetworkAuthenticationViewController.swift
//  Engineer_iOS
//
//  Created by Macstorm on 26.10.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit
import ReactiveCocoa
import CloudKit
import PKHUD

class NetworkAuthenticationViewController: UIViewController {

    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginTextField: UITextField!
    
    let networkContainer = NetworkDataContainer.instance;
    
    let getUserIDFlag = MutableProperty<Bool>(false)
    let getUserPasswordsFlag = MutableProperty<Bool>(false)
    let getAllGamesFlag = MutableProperty<Bool>(false)
    
    var usageFirstFlag = true
    var usageSecondFlag = true
    
    var user: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.racObserver()
        
        self.networkContainer.deviceUser = self.user
        
        self.loadDataBase()
        
        HUD.show(.Progress)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        self.getAllGamesFlag.value = false;
        self.getUserIDFlag.value = false;
        self.getUserPasswordsFlag.value = false;
        
    }
    override func viewWillAppear(animated: Bool) {
      
        self.usageFirstFlag = true
        self.usageSecondFlag = true
        
    }
    
    func loadDataBase(){
        

        self.getUserID();
        
        
        self.getUserIDFlag.producer.combineLatestWith(self.getUserPasswordsFlag.producer).combineLatestWith(self.getAllGamesFlag.producer).on(started: nil, event: nil, failed: nil, completed: nil, interrupted: nil, terminated: nil, disposed: nil) { (firstAndSecond : (Bool, Bool), third) in
            
            if self.usageFirstFlag{
                
                if firstAndSecond.0.boolValue{
                    
                    self.getUserPasswords()
                    self.getAllGames();
                    
                    self.usageFirstFlag = false
                }
            }
            
            if self.usageSecondFlag{
                
                if firstAndSecond.0.boolValue && firstAndSecond.1.boolValue && third.boolValue{
                    
                    print("DB fetch completed")
                    
                    HUD.hide()
                    
                    self.usageSecondFlag = false
                }
            }
            
            
        }.start()
        
    }
    
    func getAllGames(){
        
        let query = CKQuery(recordType: "Games", predicate: NSPredicate(format: "TRUEPREDICATE"))
        
        query.sortDescriptors = [NSSortDescriptor(key: "createdAt", ascending: false)]
        
        self.networkContainer.games = [CKRecord]()
        self.networkContainer.masterAccounts = [CKRecord]()
        
        self.networkContainer.publicDB.performQuery(query, inZoneWithID: nil) { (records: [CKRecord]?, error: NSError?) in
            
            if error == nil{
                
                guard records?.count > 0 else{
                    print("No games at DB")
                    self.getAllGamesFlag.value = true
                    return
                }
                
                var counter = 0
                
                for object in records!{
                    
                    self.networkContainer.games?.append(object)
                    
                    self.networkContainer.publicDB.fetchRecordWithID((object.valueForKey("masterAccount")?.recordID)!, completionHandler: { (record : CKRecord?, error : NSError?) in
                        
                        if error == nil{
                            
                            guard record != nil else{
                                
                                print("Nill value at NewtorkAuthenticationViewController -> getAllGames fetchMasterAcccount")
                                return
                            }
                            
                            self.networkContainer.masterAccounts?.append(record!)
                            counter += 1
                            
                            if counter == records?.count{
                                
                                self.getAllGamesFlag.value = true
                            }
                            
                        }else{
                            
                            print("Error at NewtorkAuthenticationViewController -> getAllGames fetch masterAccount")
                            
                        }
                    })
                    
                }
                
                
            }else{
                
                print("Error at NewtorkAuthenticationViewController -> getAllGames")
            }
        }
    }
    func getUserPasswords(){
        
        let query = CKQuery(recordType: "Passwords", predicate: NSPredicate(format: "TRUEPREDICATE"))
        
        self.networkContainer.passwords = [CKRecord]()
        
        self.networkContainer.privateDB.performQuery(query, inZoneWithID: nil, completionHandler: { (records: [CKRecord]?, error: NSError?) in
            
            if error == nil{
                
                guard records?.count != 0 else{
                    
                    print("No passwords")
                    print("No accounts created")

                    self.getUserPasswordsFlag.value = true;
                    
                    return
                }
                
                
                
                for object in records!{
                    
                   self.networkContainer.passwords?.append(object)
                    
                }
                
                self.getUserPasswordsFlag.value = true;
                
            }else{
                
                print("Error at AuthenticationViewController -> getUserPasswords")
            }
        })
        
    }
    func getUserID(){
        
        
        self.networkContainer.defaultDB.fetchUserRecordIDWithCompletionHandler { (recordID: CKRecordID?, error: NSError?) in
            
            if error == nil{
                
                guard recordID != nil else{
                    
                    print("nil record at NetworkAuthenticationController -> getUserID")
                    return
                }
                
                self.networkContainer.privateDB.fetchRecordWithID(recordID!, completionHandler: { (record: CKRecord?, error:NSError?) in
                    
                    if error == nil{
                        
                        guard record != nil else{
                            
                            print("No user record in DB")
                            return
                        }
                        
                        self.networkContainer.userRecord = record
                        self.getUserIDFlag.value = true
                        
                    }else{
                        
                        print("Error at NetworkAuthenticationController -> getUserID privateDB")
                    }
                    
                })
                
                
            }else{
                
                print("Error at NetworkAuthenticationController -> getUserID")
                print(error?.localizedDescription)
            }
        }
    }
    func racObserver(){
        
        
        
        self.logInButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside).subscribeNext {
            
            _ in
            
            self.logInAtCloudKit()
            
        }
        
        self.createAccountButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside).subscribeNext{
            
            _ in
            
           self.registerAtCloudKit()
            
        }
        
        let loginSignal = loginTextField.rac_textSignal().map{ (text: AnyObject!) -> AnyObject! in
            
            let textString = text as! String
            
            return (textString as NSString).length >= 0   // !!!!! ZMIENIC NA 5
            
        }
        
        let passwordSignal = passwordTextField.rac_textSignal().map{ (text: AnyObject!) -> AnyObject! in
            
            let textString = text as! String
            
            return (textString as NSString).length >= 0 // !!!!! ZMIENIC NA 5
            
        }
        
        let validationSignal = RACSignal.combineLatest([loginSignal, passwordSignal]).map{
            
            let tuple = $0 as! RACTuple
            let login = tuple.first as! Bool
            let password = tuple.second as! Bool
            
            return login && password
            
        }
        
        validationSignal.subscribeNext({
            
            (result: AnyObject!) in
            
            self.logInButton.enabled = false
            self.logInButton.alpha = 0.3
            
            self.createAccountButton.enabled = false
            self.createAccountButton.alpha = 0.3
            
            
            let resultAsBool = result as! Bool
            
            if resultAsBool{
                
            
                self.logInButton.enabled = true
                self.logInButton.alpha = 1.0
                
                
                self.createAccountButton.enabled = true
                self.createAccountButton.alpha = 1.0
                
            }
            
        })
        
        
    }
    
    func logInAtCloudKit(){
        
        var indicator = false
        var passwordRecord: CKRecord?
        
        for object in self.networkContainer.passwords!{
            
            if self.passwordTextField.text! == (object.objectForKey("password") as! String){
                
                indicator = true
                passwordRecord = object
                
                break
            }
        }
        
        guard indicator else{
            
            print("Wrong password")
            return
        }
        
        let query = CKQuery(recordType: "Accounts", predicate: NSPredicate(format: "user == %@ && name == %@ && password == %@", self.networkContainer.userRecord!, self.loginTextField.text!, passwordRecord!))
        
        self.networkContainer.publicDB.performQuery(query, inZoneWithID: nil) { (records: [CKRecord]?, error: NSError?) in
            
            if error == nil{
                
                if records?.count == 0{
                    
                    print("No record in database")
                    
                }else{
                    
                    self.networkContainer.accountRecord = records?.first!
                    print("User logged")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        self.performSegueWithIdentifier("NetworkGamesSegue", sender: self)
                    })
                    
                    
                }
            }else{
                
                print("Error -> logInAtCloudKit")
                print(error?.description)
            }
        }
        
    }
    
    func registerAtCloudKit(){
        
        guard self.networkContainer.userRecord != nil else{
            
            print("Error -> registerAtCloudKit -> no user record")
            return
        }
        
        let query = CKQuery(recordType: "Accounts", predicate: NSPredicate(format: "TRUEPREDICATE"))
        
        self.networkContainer.publicDB.performQuery(query, inZoneWithID: nil) { (records: [CKRecord]?, error: NSError?) in
            
            if error == nil{
                
                guard records != nil else{
                    
                    print("Error at AuthenticationViewController -> registerAtCK -> truepredicate query NIL RECORDS")
                    return
                }
                
                if records?.count != 0{
                    
                    var indicator = false
                    
                    for object in records!{
                        
                        let name = object.objectForKey("name") as! String
                        
                        if self.loginTextField.text! == name{
                            
                            indicator = true
                            break
                        }
                    }
                    
                    guard !indicator else{
                        
                        print("That name already exists")
                        return
                    }
                    
                    self.processRegistration()
                    
                }else{
                    
                    self.processRegistration()
                }
            }else{
                
                print("Error at AuthorizationViewController -> registerAtCK -> truepredicate query")
            }
        }
        
    }
    
    func saveAccountRecord(passwordReference: CKReference){
        
        let accountRecord = CKRecord(recordType: "Accounts")
        let userReference = CKReference(record: self.networkContainer.userRecord!, action: .DeleteSelf)
        
        
        accountRecord.setObject(self.loginTextField.text!, forKey: "name")
        accountRecord.setObject(userReference, forKey: "user")
        accountRecord.setObject(passwordReference, forKey: "password")
        
        self.networkContainer.publicDB.saveRecord(accountRecord, completionHandler: { (record: CKRecord?, error: NSError?) in
            
            if error == nil{
                
                guard record != nil else{
                    
                    print("Error at AuthenticationViewController -> processRegistration -> account save NIL RECORD")
                    return
                }
                
                print("Account saved!")
                
            }else{
                
                print("Error at AuthenticationViewController -> processRegistration -> account save")
            }
        })
    }
    func processRegistration(){
        
        var passwordReference: CKReference?
        
        for object in self.networkContainer.passwords!{
            
            if self.passwordTextField.text! == (object.valueForKey("password") as! String){
                
                passwordReference = CKReference(record: object, action: .DeleteSelf)
                break
                
            }
        }
        
        if passwordReference != nil{
            
            self.saveAccountRecord(passwordReference!)
            
            dispatch_async(dispatch_get_main_queue(), {
                
                self.performSegueWithIdentifier("NetworkGamesSegue", sender: self)
            })
            
            
        }else{
            
            let passwordRecord = CKRecord(recordType: "Passwords")
            
            passwordRecord.setObject(self.passwordTextField.text!, forKey: "password")
            
            self.networkContainer.privateDB.saveRecord(passwordRecord) { (record: CKRecord?, error: NSError?) in
                
                if error == nil{
                    
                    guard record != nil else{
                        
                        print("Error at AuthorizationViewController -> processRegistration -> pass save NIL RECORD")
                        return
                    }
                    
                    print("Password saved!")
                    
                    self.networkContainer.passwords?.append(record!)
                    
                    passwordReference = CKReference(record: record!, action: .DeleteSelf)
                    
                    self.saveAccountRecord(passwordReference!)
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        self.performSegueWithIdentifier("NetworkGamesSegue", sender: self)
                    })
                    

                }else{
                    
                    print("Error at AuthorizationViewController -> processRegistration -> pass save")
                }
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
