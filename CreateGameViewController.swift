//
//  CreateGameViewController.swift
//  Engineer_iOS
//
//  Created by Macstorm on 27.10.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit
import CloudKit
import ReactiveCocoa
import PKHUD
import CoreData

class CreateGameViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
   
    @IBOutlet weak var adventurerCardTextField: UITextField!
    @IBOutlet weak var adventurerCardSelector: UIPickerView!
    @IBOutlet weak var passwordTextField: UITextField!

    @IBOutlet weak var nameTextField: UITextField!
    
    let networkContainer = NetworkDataContainer.instance
    var pickedAdventurerCard: AdventurerCard?
    weak var tableViewReference: UITableView?
    var delegate: AppDelegate?
    
    var returnCounter: Int!
    var targetCounter: Int!
    
    struct DependencyTablesStructure{
        
        
        var dependencyTableObject: NSManagedObject!
        var dependencyTableReferenceRecord: CKReference!
        
        
    }
    
    var dependencyTablesStructure: [DependencyTablesStructure]?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        // Do any additional setup after loading the view.

    }
 
    override func viewWillAppear(animated: Bool) {
        
        self.adventurerCardSelector.hidden = true
        self.returnCounter = 0
        self.targetCounter = 0
        

    }
    
    @IBAction func cancel(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    
    }
    
    @IBAction func createButton(sender: AnyObject) {
        
        guard self.networkContainer.checkNetworkConnection() else{
            
            print("No network")
            return
        }
        
        guard self.networkContainer.checkCloudKitConnection() else{
            
            print("No CloudKit")
            return
        }
        
        guard self.networkContainer.games != nil else{
            
            return
        }
        
        guard self.nameTextField.text != "" else{
            
            print("No name")
            return
        }
        
        guard self.passwordTextField.text != "" else{
            
            print("No password")
            return
            
        }
        guard self.adventurerCardTextField.text != "" else{
        
            print("No ADV CARD choosen")
            return
        }
        
        for object in self.networkContainer.games!{
            
            if (object.valueForKey("name") as! String) == self.nameTextField.text!{
                
                print("Name already used")
                return
            }
        }
        
      HUD.show(.Progress)
        
      let gameRecord = CKRecord(recordType: "Games")
        
        gameRecord.setObject(self.nameTextField.text!, forKey: "name")
        gameRecord.setObject(self.passwordTextField.text!, forKey: "password")
        
        let accountReference = CKReference(record: self.networkContainer.accountRecord!, action: .DeleteSelf)
        gameRecord.setObject(accountReference, forKey: "masterAccount")
        
        gameRecord.setObject(NSDate(), forKey: "createdAt")
        
        self.networkContainer.publicDB.saveRecord(gameRecord) { (record: CKRecord?, error: NSError?) in
            
            if error == nil{
                
                guard record != nil else{
                    
                    print("Nil record at CreateGameVC -> createButton action")
                    return
                }
                
                print("Saved!")
                
                /*let adventurerCardRecord = CKRecord(recordType: "AdventurerCards")
                adventurerCardRecord.setObject(self.pickedAdventurerCard!.name, forKey: "name")*/
                
                        let gameReference = CKReference(record: gameRecord, action: .DeleteSelf)
                
                        let adventurerCardRecord = CKRecord(recordType: "AdventurerCards")
                        
                        adventurerCardRecord.setObject(self.pickedAdventurerCard!.name, forKey: "name")
                        adventurerCardRecord.setObject(gameReference, forKey: "game")
                        
                        
                        self.networkContainer.publicDB.saveRecord(adventurerCardRecord, completionHandler: { (record: CKRecord?, error: NSError?) in
                            
                            if error == nil{
                                
                                guard record != nil else{
                                    
                                    print("Nil value at CreateGameVC -> createButton action -> save ADV CARD record")
                                    return
                                }
                                
                                print("ADV CARD saved!")
                                
                                self.networkContainer.games?.insert(gameRecord, atIndex: 0)
                                self.networkContainer.masterAccounts?.insert(self.networkContainer.accountRecord!, atIndex: 0)
                                
                                //self.networkContainer.games?.append(gameRecord)
                                //self.networkContainer.masterAccounts?.append(self.networkContainer.accountRecord!)

                                
                                let adventurerCardReference = CKReference(record: adventurerCardRecord, action: .DeleteSelf)
                                
                                self.getSpecifiedDataForPickedAdventurerCardAndSave(adventurerCardReference)
                                
                                
                                
                            }else{
                                
                                print("Error at CreateGameVC -> createButton action -> save ADV CARD record")
                            }
                        })

                
                
            }else{
                
                print("Error at CreateGameVC -> createButton action")
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        guard self.networkContainer.deviceUser!.adventurerCards != nil else{
            
            return 0
        }
        
        return (self.networkContainer.deviceUser!.adventurerCards?.count)!
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
       return self.networkContainer.deviceUser!.adventurerCards![row].name!
        
    
    }
    
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
     
        guard self.networkContainer.deviceUser?.adventurerCards != nil else{
            
            return
        }
        
        self.adventurerCardTextField.text = self.networkContainer.deviceUser?.adventurerCards![row].name
        
        // self.dropDownList.hidden = true
        self.pickedAdventurerCard = self.networkContainer.deviceUser?.adventurerCards![row]
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
       guard self.networkContainer.deviceUser?.adventurerCards != nil else{
        
            return
        }
        
        self.view.endEditing(true)
        self.adventurerCardSelector.hidden = false
    }
    
    func getSpecifiedDataForPickedAdventurerCardAndSave(adventurerCardReference: CKReference!){
     
        let fetchQuery = NSFetchRequest(entityName: "AdventurerCard")
        let predicate = NSPredicate(format: "user == %@ && name == %@", self.networkContainer.deviceUser!.userObject!, (self.pickedAdventurerCard?.name!)!)
        
        fetchQuery.predicate = predicate
        
        self.pickedAdventurerCard?.adventurerObject = nil
        
        do{
            
            let records = try self.delegate?.managedObjectContext.executeFetchRequest(fetchQuery)
            
            guard records != nil else{
                
                print("Error ar CreateGameVC -> getSpecifiedDataForPickedADVCard")
                return
            }
            
            
            self.pickedAdventurerCard?.adventurerObject = records?.first as? NSManagedObject
            
        }catch{
            
            let error = error as NSError
            print(error.localizedDescription)
        }
        
        guard self.pickedAdventurerCard?.adventurerObject != nil else{
            
            print("Nil ADV CARD object at CreateGameVC -> getSpecified...")
            return
        }
        
        let fatherObject = self.pickedAdventurerCard?.adventurerObject?.valueForKey("dependencyTable")
        

        let fatherRecord = CKRecord(recordType: "DependencyTables")
        
        fatherRecord.setObject(fatherObject?.valueForKey("name") as! String, forKey: "name")
        fatherRecord.setObject(adventurerCardReference, forKey: "adventurerCard")
        
        self.networkContainer.publicDB.saveRecord(fatherRecord) { (record: CKRecord?, error: NSError?) in
            
            if error == nil{
                
                guard record != nil else{
                    
                    print("Nil record at CreateGameVC -> getSpecfiedData...")
                    return
                }
                
                let fatherReference = CKReference(record: fatherRecord, action: .DeleteSelf)
                
                self.dependencyTablesStructure = [DependencyTablesStructure]()
                
                self.findDependencyStructureAndSave(fatherObject as! NSManagedObject, fatherReference: fatherReference, adventurerCardReference: adventurerCardReference)
                
            }else{
                
                print("Error at CreateGameVC -> getSpecfiedData...")
            }
        }
        
    }

    func findDependencyStructureAndSave(father: NSManagedObject!, fatherReference: CKReference!, adventurerCardReference: CKReference!){
        
        
        let fetchQuery = NSFetchRequest(entityName: "DependencyTable")
        let predicate = NSPredicate(format: "father == %@", father)
        
        fetchQuery.predicate = predicate
        
        do{
            
            let records = try self.delegate?.managedObjectContext.executeFetchRequest(fetchQuery)
            
            guard records != nil else{
                
                print("error!")
                return
            }
            
            
            for object in records!{
                
                self.targetCounter = self.targetCounter + 1
                
                let childRecord = CKRecord(recordType: "DependencyTables")
                
                childRecord.setObject(object.valueForKey("name") as! String, forKey: "name")
                childRecord.setObject(adventurerCardReference, forKey: "adventurerCard")
                
                if let availability = object.valueForKey("availability") as? Int{
                    
                    childRecord.setObject(availability, forKey: "availability")
                }
                
                childRecord.setObject(fatherReference, forKey: "father")
                
                self.networkContainer.publicDB.saveRecord(childRecord) { (record: CKRecord?, error: NSError?) in
                    
                    if error == nil{
                        
                        guard record != nil else{
                            
                            print("Nil record at CreateGameVC -> findDepStr...")
                            return
                        }
                        
                        let childReference = CKReference(record: childRecord, action: .DeleteSelf)
                        
                        self.dependencyTablesStructure?.append(DependencyTablesStructure(dependencyTableObject: object as! NSManagedObject, dependencyTableReferenceRecord: childReference))
                        
                        
                        self.findDependencyStructureAndSave(object as! NSManagedObject, fatherReference: childReference, adventurerCardReference: adventurerCardReference)
                        
                        print("Saved deps str..")
                        
                        self.returnCounter = self.returnCounter + 1
                        
                        if self.returnCounter == self.targetCounter{
                            
                            print("It is done!")
                            
                            self.findAllPropertiesWithDependenciesAndSave(self.pickedAdventurerCard?.adventurerObject!, adventurerCardReference: adventurerCardReference)

                        }
                    }else{
                        
                        print("Error at CreateGameVC -> findDepStr...")
                    }
                }

                
            }
            
        }catch{
            
            let error = error as NSError
            print(error.localizedDescription)
        }
        
    }
    
    
    func findAllPropertiesWithDependenciesAndSave(adventurerCard: NSManagedObject!, adventurerCardReference: CKReference!){
        
        let fetchQuery = NSFetchRequest(entityName: "Property")
        let predicate = NSPredicate(format: "adventurerCard == %@", adventurerCard)
        
        fetchQuery.predicate = predicate
        
        var localCounter = 0
        var localTargetCounter = 0
        
        do{
            
            let results = try self.delegate?.managedObjectContext.executeFetchRequest(fetchQuery)
            
            guard results != nil else{
                
                print("Nil results coreData at CreateGameVC -> findAllProp..")
                return
            }
            
            for object in results!{
                
                let record = CKRecord(recordType: "Properties")
                record.setObject(object.valueForKey("name") as! String, forKey: "name")
                record.setObject(object.valueForKey("baseValue") as! Int, forKey: "baseValue")
                record.setObject(adventurerCardReference, forKey: "adventurerCard")
                
                self.networkContainer.publicDB.saveRecord(record, completionHandler: { (record: CKRecord?, error:NSError?) in
                    
                    if error == nil{
                        
                        guard record != nil else{
                            
                            print("Nil results cloudKit at CreateGameVC -> findAllProp..")
                            return
                        }
                        
                        let trueValue = true
                        let fetchQuery = NSFetchRequest(entityName: "Dependency")
                        let predicate = NSPredicate(format: "property == %@ && isAvailable == %@", object as! NSManagedObject, trueValue)
                        
                        fetchQuery.predicate = predicate
                        
                        do{
                            
                            let results = try self.delegate?.managedObjectContext.executeFetchRequest(fetchQuery)
                            
                            guard results != nil else{
                                
                                print("Nil results coreData -> dependencies at CreateGameVC -> findAllProp..")
                                return
                            }
                            
                            let propertyReference = CKReference(record: record!, action: .DeleteSelf)
                            
                            for object in results!{
                                
                                let innerRecord = CKRecord(recordType: "Dependencies")
                                innerRecord.setObject(propertyReference, forKey: "property")
                                
                                let dependencyTableReferenceObject = object.valueForKey("dependencyTable") as! NSManagedObject
                                
                                var objectFound = false
                                
                                for dependencyTableStructureObject in self.dependencyTablesStructure!{
                                    
                                    if dependencyTableStructureObject.dependencyTableObject == dependencyTableReferenceObject{
                                        
                                        innerRecord.setObject(dependencyTableStructureObject.dependencyTableReferenceRecord, forKey: "dependencyTable")
                                        
                                        objectFound = true
                                        break
                                    }
                                    
                                }
                                
                                guard objectFound == true else{
                                    
                                    print("No reference of dependencyTable at CreateGameVC -> findAllProp.. dependencySave")
                                    return
                                }
                                
                                innerRecord.setObject(object.valueForKey("equationType") as! String, forKey: "equationType")
                                innerRecord.setObject(object.valueForKey("name") as! String, forKey: "name")
                                
                                innerRecord.setObject(object.valueForKey("value") as! Int, forKey: "value")
                                
                                self.networkContainer.publicDB.saveRecord(innerRecord, completionHandler: { (record :CKRecord?, error: NSError?) in
                                    
                                    if error == nil{
                                        
                                        guard record != nil else{
                                            
                                             print("Nil record at CreateGameVC -> findAllProp... dependencySave")
                                             return
                                        }
                                        
                                        print("Props Saved!!")
                                        localCounter += 1
                                        
                                        if localCounter == localTargetCounter{
                                            
                                            HUD.hide()
                                            
                                            dispatch_async(dispatch_get_main_queue(), {
                                                
                                                self.tableViewReference?.reloadData()
                                                self.dismissViewControllerAnimated(true, completion: nil)
                                                
                                            })
                                        }
                                        
                                    }else{
                                        
                                        print("Error at CreateGameVC -> findAllProp... dependencySave")
                                    }
                                })
                                
                                localTargetCounter += 1
                            }
                            
                            localCounter += 1
                            
                        }catch{
                            
                            let error = error as NSError
                            print(error.localizedDescription)
                        }
                        
                        
                    }else{
                        
                        print("Error at CreateGameVC -> findAllProp")
                    }
                })
                
                localTargetCounter += 1
            }
            
        }catch{
            
            let error = error as NSError
            print(error.localizedDescription)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
