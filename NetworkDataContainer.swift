//
//  NetworkDataContainer.swift
//  Engineer_iOS
//
//  Created by Macstorm on 26.10.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit
import CloudKit

class NetworkDataContainer: NSObject {
    
    let publicDB = CKContainer.defaultContainer().publicCloudDatabase
    let privateDB = CKContainer.defaultContainer().privateCloudDatabase
    let defaultDB = CKContainer.defaultContainer()
    
    var userRecord: CKRecord?
    var passwords: [CKRecord]?
    var accountRecord: CKRecord?
    var games: [CKRecord]?
    var masterAccounts: [CKRecord]?
    var deviceUser: User?
    
    
    static let instance = NetworkDataContainer();
    
    func checkNetworkConnection() -> Bool{
        
        return true
        
    }
    
    func checkCloudKitConnection() -> Bool{
        
        return true
    }
}
