//
//  AdventurerCard.swift
//  Engineer_iOS
//
//  Created by Macstorm on 19.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import Foundation
import CoreData

class AdventurerCard{
    
    var dependencyTable: DependencyTable?
    var properties: [Property]?
    
    var name: String!
    var createdAt: NSDate!
    var adventurerObject: NSManagedObject?
    
    init(name: String!, createdAt: NSDate!){
        
        self.name = name
        self.createdAt = createdAt
        
    }

   
    class func searchForAdventurerCards(user: User!) -> [AdventurerCard]? {
  
       let fetchRequest = NSFetchRequest(entityName: "AdventurerCard")
       let predicate = NSPredicate(format: "user == %@", user.userObject!)
       let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: false)
        
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do{
            let results = try user.userObject?.managedObjectContext?.executeFetchRequest(fetchRequest)
            
            var temporaryTable = [AdventurerCard]()
            
            for object in results!{
                
                temporaryTable.append(AdventurerCard(name: object.valueForKey("name") as! String, createdAt: object.valueForKey("createdAt") as! NSDate))
            }
            
            return temporaryTable
        }catch{
            
            let error = error as NSError
            
            print("Error at AdventurerCard -> searchForAdventurerCards")
            print(error.userInfo)
            return nil
        }
    }
    
}