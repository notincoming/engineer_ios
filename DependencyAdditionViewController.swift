//
//  CustomAlertViewController.swift
//  Engineer_iOS
//
//  Created by Macstorm on 15.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit
import ReactiveCocoa


class DependencyAdditionViewController: UIViewController {

    @IBOutlet weak var numberTextField: UITextField!
    
    @IBOutlet var titleField: [UILabel]!
    @IBOutlet var nameTextField: [UITextField]!
    @IBOutlet var saveButton: [UIButton]!
    @IBOutlet var cancelButton: [UIButton]!
    

    enum Variant{
        
        case nameOnly
        case nameAndNumber
    }
    
    var variant: Variant?
    var racSignalName: RACSignal?
    var racSignalNumber: RACSignal?
    var completionWithName: ((String) -> Void)?
    var completionWithNameAndNumber: ((String, Int) -> Void)?
    
    @IBOutlet weak var viewWithNameAndNumber: UIView!
    @IBOutlet weak var viewWithName: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       self.view.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.5)
        
        self.viewWithNameAndNumber.layer.masksToBounds = false;
        self.viewWithNameAndNumber.layer.shadowOffset = CGSizeMake(0, 0);
        self.viewWithNameAndNumber.layer.shadowRadius = 5;
        self.viewWithNameAndNumber.layer.shadowOpacity = 0.5;
        
        self.viewWithName.layer.masksToBounds = false;
        self.viewWithName.layer.shadowOffset = CGSizeMake(0, 0);
        self.viewWithName.layer.shadowRadius = 5;
        self.viewWithName.layer.shadowOpacity = 0.5;
    }

    
    override func viewWillAppear(animated: Bool) {
        
        switch self.variant!{
            
        case Variant.nameOnly:
            self.viewWithNameAndNumber.hidden = true
     
        case Variant.nameAndNumber:
            self.viewWithName.hidden = true
        }
        
        
        for object in self.titleField!{
            
            if object.tag == 0{
                
               object.text = "New table"
            }
            else{
                
               object.text = "New dependency"
            }
        }
        self.racObserver()
        
    }
    
    override func viewDidDisappear(animated: Bool) {
        
        self.viewWithNameAndNumber.hidden = false
        self.viewWithName.hidden = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    func completionHandler() -> Void{
        
        switch self.variant!{
            
        case Variant.nameOnly:
            
            guard self.completionWithName != nil else{
                return
            }
            
            var textField: UITextField!
            
            for object in self.nameTextField!{
                
                if object.tag == 0{
                    
                    textField = object
                }
            }
            
            self.completionWithName!(textField.text!)
        
        case Variant.nameAndNumber:
            
            guard self.completionWithNameAndNumber != nil else{
                return
            }
            
            var textField: UITextField!
            
            for object in self.nameTextField!{
                
                if object.tag == 1{
                    
                    textField = object
                }
            }
            self.completionWithNameAndNumber!(textField.text!, Int(self.numberTextField.text!)!)
        }
        
        
    }
    
    func racObserver() -> Void{
        
        switch self.variant!{
            
        case Variant.nameAndNumber:
            
            
            for object in self.nameTextField!{
                
                if object.tag == 1{
                    
                   self.racSignalName = RACWrapper.racForTextField(object, lengthConstraint: 0, isIntConstraint: false)
 
                }
            }

            
            self.racSignalNumber = RACWrapper.racForTextField(self.numberTextField, lengthConstraint: 0, isIntConstraint: true)

            
            let validationSignal = RACSignal.combineLatest([self.racSignalName!, self.racSignalNumber!]).map{
                
                let tuple = $0 as! RACTuple
                let name = tuple.first as! Bool
                let value = tuple.second as! Bool
                
                return name && value
            }
            
            validationSignal.subscribeNext({
            
                result in
                
                let boolValue = result as! Bool
                
                if boolValue{
                    
                    
                    for object in self.saveButton!{
                        
                        object.enabled = true
                    }
                }
                else{
                    
                    for object in self.saveButton!{
                        
                        object.enabled = false
                    }
                }
            
            })
            
        case Variant.nameOnly:
            
            for object in self.nameTextField!{
                
                if object.tag == 0{
                    
                    
                   self.racSignalName = RACWrapper.racForTextField(object, lengthConstraint: 0, isIntConstraint: false)
                    
                    
                }
            
            }
         
            
            self.racSignalName?.subscribeNext({
            
                result in
                
                let boolValue = result as! Bool
                
                if boolValue{
                    
                    for object in self.saveButton!{
                        
                        object.enabled = true
                    }
                }
                else{
                    
                    for object in self.saveButton!{
                        
                        object.enabled = false
                    }
                }
           })
        }
        
        for object in self.saveButton!{
            
            object.rac_signalForControlEvents(UIControlEvents.TouchUpInside).subscribeNext({
                
                _ in
                
                print("Save")
                self.completionHandler()
                
                self.dismissViewControllerAnimated(false, completion: nil)
            })
        }
        
        for object in self.cancelButton!{
            
            object.rac_signalForControlEvents(UIControlEvents.TouchUpInside).subscribeNext({
                
                _ in
                
                print("Cancel")
                self.dismissViewControllerAnimated(false, completion: nil)
            })
        }
        
      
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
