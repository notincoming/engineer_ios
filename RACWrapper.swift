//
//  RACTextSignal.swift
//  Engineer_iOS
//
//  Created by Macstorm on 15.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import Foundation
import ReactiveCocoa

class RACWrapper{
    

   class func racForTextField(textField: UITextField!, lengthConstraint: Int?, isIntConstraint: Bool!) -> RACSignal?{
        
        if isIntConstraint && lengthConstraint != nil{
            
            let signal = textField.rac_textSignal().map{ (text: AnyObject!) -> AnyObject! in
                
                let textString = text as! String
                let numValue = Int(textString)
                
                return (textString as NSString).length > lengthConstraint! && numValue != nil
                
            }
            
            return signal!
        }
        else if !isIntConstraint{
            
          let signal = textField.rac_textSignal().map{ (text: AnyObject!) -> AnyObject! in
                
                let textString = text as! String
                
                return (textString as NSString).length > lengthConstraint!
                
            }
          
            return signal!
        }
        
        return nil
    }
    
    
}