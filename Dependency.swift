//
//  File.swift
//  Engineer_iOS
//
//  Created by Macstorm on 20.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import Foundation
import CoreData

class Dependency{
    
    var name: String!
    var dependencyTableObject: NSManagedObject!
    var equationType: String!
    var propertyObject: NSManagedObject!
    var value: Int!
    var isAvailable: Bool!
    var createdAt: NSDate!
    var dependencyObject: NSManagedObject?
    
    init(name: String!, dependencyTableObject: NSManagedObject!, equationType: String!, propertyObject: NSManagedObject!, value: Int!, isAvailable: Bool!, createdAt: NSDate!, dependencyObject: NSManagedObject?){
        
        
        self.name = name
        self.dependencyTableObject = dependencyTableObject
        self.equationType = equationType
        self.propertyObject = propertyObject
        self.value = value
        self.isAvailable = isAvailable
        self.createdAt = createdAt
        
        guard dependencyObject != nil else{
            
            return
        }
        
        self.dependencyObject = dependencyObject
    }
    
    
}