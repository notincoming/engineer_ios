//
//  CustomAlertViewController2.swift
//  Engineer_iOS
//
//  Created by Macstorm on 19.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit
import ReactiveCocoa

class AdventurerCardAdditionViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    

    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var dependencyTableTextField: UITextField!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var dropDownList: UIPickerView!

    var user: User!
    var completion: ((String, DependencyTable?) -> Void)?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.racObserver()
    

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboardAndDropDown")
        
        self.view.addGestureRecognizer(tap)
    }

    func dismissKeyboardAndDropDown(){
        
        self.view.endEditing(true)
        self.view.resignFirstResponder()
        self.dropDownList.hidden = true
        print("hiding keyboard")
    }
    
    func racObserver(){

        // MARK: TO DO , NO NIL IN TEXT ????
        
        self.saveButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside).subscribeNext({
        
            _ in
            
            guard (self.nameTextField.text != nil || self.nameTextField.text == "") && self.completion != nil else{
                return
            }

            if self.dependencyTableTextField.text != nil && self.dependencyTableTextField.text != ""{
                
                print(self.dependencyTableTextField.text!)
                self.completion!(self.nameTextField.text!, self.user.dependencyTables![self.dropDownList.selectedRowInComponent(0)])
                
                self.dependencyTableTextField.text = ""
                
                
            }
            else{
                
              self.completion!(self.nameTextField.text!, nil)
                
            }
            
            self.dismissViewControllerAnimated(false, completion: nil)
        })
        
        self.cancelButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside).subscribeNext({
        
            _ in
            
            self.dismissViewControllerAnimated(false, completion: nil)
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        
        self.dependencyTableTextField.text = ""
        
        guard self.user.dependencyTables != nil else{
            
            self.dependencyTableTextField.userInteractionEnabled = false
            return
        }
       
        

    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        guard self.user.dependencyTables != nil else{
            
            return 0
        }
    
        return (self.user.dependencyTables?.count)!
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        return self.user.dependencyTables![row].name
    }
    
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        guard self.user.dependencyTables != nil else{
            
            return
        }
        self.dependencyTableTextField.text = self.user.dependencyTables![row].name
       // self.dropDownList.hidden = true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        guard self.user.dependencyTables != nil else{
            return
        }
        self.view.endEditing(true)
        self.dropDownList.hidden = false
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
