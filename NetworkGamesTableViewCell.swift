//
//  NetworkGamesTableViewCell.swift
//  Engineer_iOS
//
//  Created by Macstorm on 27.10.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit

class NetworkGamesTableViewCell: UITableViewCell {

    @IBOutlet weak var masterAccountLabel: UILabel!
    @IBOutlet weak var gameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
