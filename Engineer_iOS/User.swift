//
//  User.swift
//  Engineer_iOS
//
//  Created by Macstorm on 05.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import Foundation
import CoreData
import UIKit

public class User{
    
    var login: String!
    var password: String!
    var games: [Game]?
    var dependencyTables: [DependencyTable]?
    var adventurerCards: [AdventurerCard]?
    
    var delegate: AppDelegate!
    var entityName: String!
    var userObject: NSManagedObject?
    
    init(login: String!, password: String!){
        
        self.login = login
        self.password = password
        
        self.delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.entityName = "User"
        
        self.games = nil
        self.userObject = nil
        
    }
    
   func searchForLogin() -> Bool{
        
        let fetchRequest = NSFetchRequest(entityName: self.entityName)
        let prediacte = NSPredicate(format: "login == %@", self.login)
        
        fetchRequest.predicate = prediacte
        fetchRequest.fetchLimit = 1
        
        do{
            
            let result = try delegate.managedObjectContext.executeFetchRequest(fetchRequest)
            self.userObject = result.first as? NSManagedObject
        } catch let error as NSError{
            
            print("Could not fetch in User -> searchForLoginAndPasswordMatch with error \(error)")
        }
        
        if self.userObject != nil{
            
            return true
        }
        else{
            
            return false
        }
    }
    
    func createUser() -> Bool{
        
        let entityDescription = NSEntityDescription.entityForName(self.entityName, inManagedObjectContext: self.delegate.managedObjectContext)
        
        let managedObject = NSManagedObject(entity: entityDescription!, insertIntoManagedObjectContext: self.delegate.managedObjectContext)
        
        managedObject.setValue(self.login, forKey: "login")
        managedObject.setValue(self.password, forKey: "password")
        
        do {
            
            try self.delegate.managedObjectContext.save()
            self.userObject = managedObject
            
            
        } catch let error as NSError  {
            
            print("Could not save \(error), \(error.userInfo)")
            return false
        }
        
        return true
    }
    
    func searchForLoginAndPasswordMatch() -> Bool{
        
        
        let fetchRequest = NSFetchRequest(entityName: self.entityName)
        let prediacte = NSPredicate(format: "login == %@ && password == %@", self.login, self.password)
        
        fetchRequest.predicate = prediacte
        fetchRequest.fetchLimit = 1
        
        do{
            
            let result = try delegate.managedObjectContext.executeFetchRequest(fetchRequest)
            self.userObject = result.first as? NSManagedObject
            
        } catch let error as NSError{
            
            print("Could not fetch in User -> searchForLoginAndPasswordMatch with error \(error)")
        }
        
        
        if self.userObject != nil{
            
            return true
        }
        else{
            
            return false
        }
    }
    
}