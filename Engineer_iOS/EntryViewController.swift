//
//  EntryViewController.swift
//  Engineer_iOS
//
//  Created by Macstorm on 05.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit
import ReactiveCocoa
import CoreData

class EntryViewController: UIViewController {

    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var createAccountButton: UIButton!
    
    var user: User!
    
    let racHandler = RACWrapper()
    
    var alertHandler: AlertHandler!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.racEventObservation()
        self.alertHandler = AlertHandler()
        
        
       print(NSFileManager.defaultManager().URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains:NSSearchPathDomainMask.UserDomainMask).last)
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func racEventObservation(){
        
       let loginSignal = loginField.rac_textSignal().map{ (text: AnyObject!) -> AnyObject! in
            
            let textString = text as! String
            
            return (textString as NSString).length >= 0   // !!!!! ZMIENIC NA 5
            
        }
        
        let passwordSignal = passwordField.rac_textSignal().map{ (text: AnyObject!) -> AnyObject! in
            
            let textString = text as! String
            
            return (textString as NSString).length >= 0 // !!!!! ZMIENIC NA 5
            
        }
    
        let validationSignal = RACSignal.combineLatest([loginSignal, passwordSignal]).map{
            
            let tuple = $0 as! RACTuple
            let login = tuple.first as! Bool
            let password = tuple.second as! Bool
            
            self.loginField.backgroundColor = UIColor.yellowColor()
            self.passwordField.backgroundColor = UIColor.yellowColor()
            
            if login{
                
                self.loginField.backgroundColor = UIColor.whiteColor()
            }
            
            if password{
                
                self.passwordField.backgroundColor = UIColor.whiteColor()
                
            }
            return login && password
            
        }
    
        validationSignal.subscribeNext({
            
            (result: AnyObject!) in
            
            self.logInButton.enabled = false
            self.logInButton.alpha = 0.3
            
            self.createAccountButton.enabled = false
            self.createAccountButton.alpha = 0.3
            
            
            let resultAsBool = result as! Bool
            
            if resultAsBool{
                
                self.logInButton.enabled = true
                self.logInButton.alpha = 1.0
                
                self.createAccountButton.enabled = true
                self.createAccountButton.alpha = 1.0
                
                self.user = User(login: self.loginField.text!, password: self.passwordField.text!)
                
                
                self.user = User(login: "alexander", password: "olszewski")
                
            }

        })
        
        let logInSignal: RACSignal = self.logInButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside)
        logInSignal.subscribeNext({
            
            result in
            
            if self.user.searchForLoginAndPasswordMatch(){
                
                print("Logged")
                
                self.performSegueWithIdentifier("LogInSegue", sender: self)
            }
            else{
                
                print("Not logged")
                self.alertHandler.alertWithMessageForViewController("Authentication error", message: "Wrong login or password", viewController: self, completion: nil)
            }
        })
       
        let createAccountSignal: RACSignal = self.createAccountButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside)
        createAccountSignal.subscribeNext({
            
            result in
            
            if !self.user.searchForLogin(){
                
                if self.user.createUser(){
                    
                    print("Created")
                    print("Logged")
                    
                    self.performSegueWithIdentifier("LogInSegue", sender: self)
                }
                else{
                    
                    print("Not created")
                }
                
                
            }
            else{
                
                print("Not available")
                
                self.alertHandler.alertWithMessageForViewController("Username error", message: "That username already exists", viewController: self, completion: nil)
                
            }
        })
    }
    

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
     switch segue.identifier!{
            
            case "LogInSegue":
            
                let navigationController = segue.destinationViewController as! UINavigationController
                
                let viewController = navigationController.viewControllers.first as! MenuViewController
                viewController.user = self.user
            
            default:
            return
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
