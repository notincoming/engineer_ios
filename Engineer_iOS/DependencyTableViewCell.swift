//
//  DependencyTableViewCell.swift
//  Engineer_iOS
//
//  Created by Macstorm on 13.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit

class DependencyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var customLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
