//
//  AlertHandler.swift
//  PublicNews
//
//  Created by Macstorm on 31.08.2016.
//  Copyright © 2016 pl.euvic.PublicNews. All rights reserved.
//

import UIKit

public class AlertHandler: NSObject {
    
    var alertController: UIAlertController!
    var confirmation: UIAlertAction!
    var declination: UIAlertAction?
    
    public func alertWithMessageForViewController(title: String!, message: String!, viewController: UIViewController!, completion: (() -> ())?){
        
            self.alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        self.confirmation = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: {
            
            result in
            
            guard completion != nil else{
                
                return
            }
           
            
            completion!()
            
        })
        
            self.alertController.addAction(self.confirmation)
        
            self.alertController.view.setNeedsLayout()
            viewController.presentViewController(self.alertController, animated: true, completion: nil)
    }
}
