//
//  AdventurerCardCollectionViewController+FlowLayout.swift
//  Engineer_iOS
//
//  Created by Macstorm on 18.10.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import Foundation
import UIKit



extension AdventurerCardCollectionViewController: UICollectionViewDelegateFlowLayout{
    

    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        if indexPath.section == 0{
            
            let paddingSpace = sectionInsets.left * CGFloat(itemsPerRowForSection0 + 1)
            
            var availableWidth = CGFloat()
            
            if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation){
                
                availableWidth = view.frame.height - paddingSpace
            }else{
                
                availableWidth = view.frame.width - paddingSpace
            }
            
            let widthPerItem = availableWidth / CGFloat(itemsPerRowForSection0)
            
            return CGSize(width: widthPerItem, height: widthPerItem)
            
            
        }else{
            
          let paddingSpace = sectionInsets.left * CGFloat(itemsPerRowForSection1 + 1)
        
          var availableWidth = CGFloat()
            
            if UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation){
                
                availableWidth = view.frame.height - paddingSpace
            }else{
                
                availableWidth = view.frame.width - paddingSpace
            }
        
        
          let widthPerItem = availableWidth / CGFloat(itemsPerRowForSection1)
          
          return CGSize(width: widthPerItem, height: widthPerItem)
        }

    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return sectionInsets
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        return sectionInsets.left
    }
    
}