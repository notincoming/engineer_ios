//
//  Prototype.swift
//  Engineer_iOS
//
//  Created by Macstorm on 06.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import Foundation

struct DictionaryStruct{
    
    var container: [DictionaryStruct]?
    var key: String?
    
    init(){
        
        self.container = nil
        self.key = nil
        
    }
}

class Prototype{
    
    func getTheDependencyMap(name: String!, previousName: String?) -> DictionaryStruct{
        
        if previousName != nil{
            
            var elements: [String] = [] // wyniki z coreDate -> sql find table where name = name and fk = previousName
        
            var temporaryArrayOfDictionaryStruct: [DictionaryStruct] = []
            
            var temporaryDictionaryStruct = DictionaryStruct()
            
            for element in elements{
                

                temporaryDictionaryStruct = self.getTheDependencyMap("element_name" /*name from core data element*/, previousName: name)
                
                temporaryArrayOfDictionaryStruct.append(temporaryDictionaryStruct)
                
            }
            
            temporaryDictionaryStruct.container = temporaryArrayOfDictionaryStruct
            temporaryDictionaryStruct.key = name
            
            return temporaryDictionaryStruct
        
        }
        else{
            
            var temporaryDictionaryStruct = DictionaryStruct()
            temporaryDictionaryStruct.container = nil
            temporaryDictionaryStruct.key = name
            
            
            return temporaryDictionaryStruct
        }
        
    }

}