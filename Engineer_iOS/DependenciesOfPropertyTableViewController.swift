//
//  DependenciesOfPropertyTableViewController.swift
//  Engineer_iOS
//
//  Created by Macstorm on 18.10.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit
import CoreData

class DependenciesOfPropertyTableViewController: UITableViewController{
    
    var dependenciesTable: [Dependency]!
    var wasCreated: Bool?
    var delegate: AppDelegate!
    var cellsContainer: [Int : DependenciesOfPropertyTableViewCell]!

    @IBAction func cancel(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func save(sender: AnyObject) {
    
        print("save")
        
        if !(wasCreated!){
            
            for object in self.dependenciesTable{
                
                
                let managedObject = NSEntityDescription.insertNewObjectForEntityForName("Dependency", inManagedObjectContext: self.delegate.managedObjectContext)
                
                managedObject.setValue(object.createdAt, forKey: "createdAt")
                managedObject.setValue(object.equationType, forKey: "equationType")
                managedObject.setValue(object.isAvailable, forKey: "isAvailable")
                managedObject.setValue(object.name, forKey: "name")
                managedObject.setValue(object.value, forKey: "value")
                managedObject.setValue(object.dependencyTableObject, forKey: "dependencyTable")
                managedObject.setValue(object.propertyObject, forKey: "property")
                
                self.delegate.managedObjectContext.insertObject(managedObject)
                
                
            }
            
            do{
                
                try self.delegate.managedObjectContext.save()
                
            }catch{
                
                let error = error as NSError
                print("Error at DependenciesOfProperty save wasCreated == false with error: \(error.localizedDescription)")
            }
            
        }else{
            
            
            for object in self.dependenciesTable{
                
                object.dependencyObject!.setValue(object.equationType, forKey: "equationType")
                object.dependencyObject!.setValue(object.isAvailable, forKey: "isAvailable")
                object.dependencyObject!.setValue(object.value, forKey: "value")
            }
            
            do{
                
                try self.delegate.managedObjectContext.save()
            }catch{
                
                let error = error as NSError
                print("Error at DependenciesOfProperty save wasCreated == true with error: \(error.localizedDescription)")
            }
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.cellsContainer = [Int : DependenciesOfPropertyTableViewCell]()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return self.dependenciesTable.count
        
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! DependenciesOfPropertyTableViewCell
        
        
       cell.nameLabel.text = self.dependenciesTable[indexPath.row].name
       cell.availabilitySwitch.setOn(self.dependenciesTable[indexPath.row].isAvailable, animated: false)
       cell.valueTextField.text = "\(self.dependenciesTable[indexPath.row].value)"
       cell.equationTypeTextField.text = self.dependenciesTable[indexPath.row].equationType
       cell.equationTypeSelector.hidden = true
    
       cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(DependenciesOfPropertyTableViewController.hideEquationTypeSelector(_:))))
        
       /* cell.equationTypeTextField.rac_textSignal().subscribeNext { (result) in
            
            let text = result as! String
            print(text)
            
        }*/
        
        cell.pickerObserver.signal.observeNext { (result) in
            
            //print(result)
            self.dependenciesTable[indexPath.row].equationType = cell.equationTypeTextField.text!
            
        }
        cell.availabilitySwitch.rac_signalForControlEvents(UIControlEvents.ValueChanged).subscribeNext { (result) in
            
            let switchObject = result as! UISwitch
           // print(switchObject.on)
            self.dependenciesTable[indexPath.row].isAvailable = switchObject.on
            
        }
        
        cell.valueTextField.rac_textSignal().subscribeNext { (result) in
            
            let text = result as! String
            
            
            if (Int(text) != nil){
                
                self.dependenciesTable[indexPath.row].value = Int(text)
                
            }else if text != ""{
                
                cell.valueTextField.text = "\(self.dependenciesTable[indexPath.row].value)"
            }
            
        }
        return cell
    }
    
    func hideEquationTypeSelector(gestureRecognizer: UITapGestureRecognizer){
        
        let cell = gestureRecognizer.view as! DependenciesOfPropertyTableViewCell
        cell.equationTypeSelector.hidden = true
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
