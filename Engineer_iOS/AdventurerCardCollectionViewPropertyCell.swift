//
//  AdventurerCardCollectionViewPropertyCell.swift
//  Engineer_iOS
//
//  Created by Macstorm on 17.10.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit

class AdventurerCardCollectionViewPropertyCell: UICollectionViewCell {
    
    @IBOutlet weak var baseValueDescription: UILabel!
    @IBOutlet weak var nameDescription: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var baseValueLabel: UILabel!
    
    @IBOutlet weak var dependenciesButton: UIButton!
    
    
    
}
