//
//  DependencyTableViewController.swift
//  Engineer_iOS
//
//  Created by Macstorm on 13.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit
import CoreData

class DependencyTableViewController: UITableViewController, NSFetchedResultsControllerDelegate{
    
    let delegate = UIApplication.sharedApplication().delegate as! AppDelegate

    var user: User?
    var fetchedResultsController: NSFetchedResultsController?
    
    var dependencyTable: [ Int: [DependencyTable] ] = [ : ]
    var referenceObject: NSManagedObject?
    var isPrimaryObject: Bool!
    var isUsed: Bool?
    
    func addCompletion(result: String?, availabilityResult: Int?){
        
        let entityDescription = NSEntityDescription.entityForName("DependencyTable", inManagedObjectContext: self.delegate.managedObjectContext)
        
        let dependencyTableRecord = NSManagedObject(entity: entityDescription!, insertIntoManagedObjectContext: self.delegate.managedObjectContext)
        
        guard result != nil else{
            
            return
        }
        
        dependencyTableRecord.setValue(result!, forKey: "name")
        dependencyTableRecord.setValue(NSDate(), forKey: "createdAt")
        dependencyTableRecord.setValue(false, forKey: "isUsed")
        
        if availabilityResult != nil{
            
            guard availabilityResult != nil else{
                
                return
            }
            
            dependencyTableRecord.setValue(availabilityResult!, forKey: "availability")
        }
       
        
        dependencyTableRecord.setValue(self.user?.userObject!, forKey: "user")
        
        if self.referenceObject != nil {
            
            let children = self.referenceObject?.mutableSetValueForKey("children")
            children?.addObject(dependencyTableRecord)
        }
        
        do{
            
            
            try self.delegate.managedObjectContext.save()
            
            
        }catch{
            
            let error = error as NSError
            print(error.userInfo)
            print("Error during save at DependencyTableViewController -> addDependencyTable")
        }
        
    }
    

    func addDependencyTable(viewController: DependencyAdditionViewController) {

            if self.referenceObject != nil && self.isPrimaryObject{
                
                viewController.completionWithNameAndNumber =  {
                    
                    [unowned self]
                    
                    (result, availabilityResult) in
                    
                    self.addCompletion(result, availabilityResult: availabilityResult)
                    
                    
                }
            }
            else{
                
                viewController.completionWithName = {
                    
                  [unowned self]
                    
                    result in
                    
                    self.addCompletion(result, availabilityResult: nil)
                }
               
            }
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        guard self.fetchedResultsController != nil else{
            
            return 0
        }
        
        return (self.fetchedResultsController?.sections?.count)!
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        

        guard self.fetchedResultsController != nil else{
            
            return 0
        }
        
        return (self.fetchedResultsController?.sections![section].numberOfObjects)!

    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! DependencyTableViewCell
        
        guard self.fetchedResultsController != nil else{
           
            return cell
        }
        
        
        cell.customLabel.text = self.dependencyTable[indexPath.section]![indexPath.row].name as String


        return cell
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.setTheData()
        print(self.isUsed)
        
    }
    
    func setTheData() -> Void{
        
        guard self.fetchedResultsController != nil else{
            
            return
        }
        
        self.fetchedResultsController?.delegate = self
        
        var iterator = 0
        //var temporaryStructureTable = [DependencyStructure]()
        
        for section in (self.fetchedResultsController?.sections)! {
            
            var temporaryTable = [DependencyTable]()
            
            for object in section.objects!{
                
                temporaryTable.append(DependencyTable(name: object.valueForKey("name") as! String, createdAt: object.valueForKey("createdAt") as! NSDate, availability: object.valueForKey("availability") as? Int, managedObject: object as? NSManagedObject, isUsed: object.valueForKey("isUsed") as? Bool))
                
                
                // temporaryStructureTable.append(DependencyStructure(key: object.valueForKey("name") as? String, container: nil))
                
            }
            
            self.dependencyTable[iterator] = temporaryTable
            
            
            iterator += 1
        }
        
        // GlobalDependencyStructreContainer.shared.dependencyStructure.container![globalKey] = temporaryStructureTable
        
        if self.referenceObject != nil{
            
            self.title = self.referenceObject?.valueForKey("name") as? String
        }
        else{
            
            self.title = "Dependency tables"
            
        }
    }
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        
       tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        
       tableView.endUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        
       switch (type) {
        case .Insert:
            
            if let indexPath = newIndexPath {
                
                let element = DependencyTable(name: anObject.valueForKey("name") as! String, createdAt: anObject.valueForKey("createdAt") as! NSDate, availability: anObject.valueForKey("availability") as? Int, managedObject: anObject as? NSManagedObject, isUsed: anObject.valueForKey("isUsed") as? Bool)
                
                self.dependencyTable[indexPath.section]?.insert(element, atIndex: 0)
                
                tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            }
            break;
        case .Delete:
            if let indexPath = indexPath {
                
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            }
            break;
        
        case .Update:
            if let indexPath = indexPath {
                
                // update stuff
            }
            break;
        case .Move:
            if let indexPath = indexPath {
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Fade)
            }
            break;
        } 
        
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        
        switch identifier{
            
            case "CustomAlertViewControllerSegue":
                
                guard self.isUsed != nil else{
                    
                    return true
                }
                
                if self.isUsed!{
                
                    return false
                    
                }else{
                    
                    return true
                }
            
            default:
            
            return true
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        switch segue.identifier!{
            
            case "CustomAlertViewControllerSegue":
              
                let viewController = segue.destinationViewController as! DependencyAdditionViewController
                
                if self.referenceObject != nil && self.isPrimaryObject{

                    viewController.variant = DependencyAdditionViewController.Variant.nameAndNumber
                }
                else{
                    
                    viewController.variant = DependencyAdditionViewController.Variant.nameOnly
                }
            
                self.addDependencyTable(viewController)
            
            
            default:
                
                let selectedIndexPath = tableView.indexPathForSelectedRow
                
                //self.referenceObject = self.fetchedResultsController!.objectAtIndexPath(selectedIndexPath!) as? NSManagedObject
                
                let referenceObjectToPass =  self.fetchedResultsController!.objectAtIndexPath(selectedIndexPath!) as? NSManagedObject
                
               /* self.fetchedResultsController?.delegate = nil
                
                
                self.fetchedResultsController = {
                    
                    
                    let fetchRequest = NSFetchRequest(entityName: "DependencyTable")
                    
                    let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: false)
                    fetchRequest.sortDescriptors = [sortDescriptor]
                    
                    let predicate = NSPredicate(format: "user == %@ && father == %@", self.user!.userObject!, self.referenceObject!)
                    
                    fetchRequest.predicate = predicate
                    
                    let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.delegate.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
                    
                    fetchedResultsController.delegate = nil
                    
                    return fetchedResultsController
                    
                }() */
                
                let fetchedResultsControllerToPass: NSFetchedResultsController = {
                    
                    
                    let fetchRequest = NSFetchRequest(entityName: "DependencyTable")
                    
                    let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: false)
                    fetchRequest.sortDescriptors = [sortDescriptor]
                    
                    let predicate = NSPredicate(format: "user == %@ && father == %@", self.user!.userObject!, referenceObjectToPass!)
                    
                    fetchRequest.predicate = predicate
                    
                    let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.delegate.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
                    
                    fetchedResultsController.delegate = nil
                    
                    return fetchedResultsController
                    
                }()
                
                do{
                    
                    try fetchedResultsControllerToPass.performFetch()
                    
                }catch{
                    
                    let error = error as NSError
                    print(error.userInfo)
                    print("Error while reinitialized fetch at DependencyTableViewController -> didSelectRow..")
                }
            
               let viewController = segue.destinationViewController as! DependencyTableViewController
               
                viewController.user = self.user
                viewController.referenceObject = referenceObjectToPass
                viewController.fetchedResultsController = fetchedResultsControllerToPass
                
                var isUsed: Bool?
                
                if self.referenceObject == nil{
                    
                    viewController.isPrimaryObject = true
                    
                    isUsed = referenceObjectToPass?.valueForKey("isUsed") as? Bool

                }else{
                    
                    isUsed = self.isUsed
                    viewController.isPrimaryObject = false
                }
            
                viewController.isUsed = isUsed
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        
        guard self.isUsed != nil else{
            
            let object = (self.dependencyTable[indexPath.section]?[indexPath.row])! as DependencyTable
            
            let isUsed = object.dependencyObject!.valueForKey("isUsed") as! Bool
            
            print(isUsed)
            
            if isUsed{
                
                return false
                
            }else{
                
                return true
            }
        }
        
        if self.isUsed!{
            
            return false
        
        }else{
            
            return true
        }
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            
            let object = (self.dependencyTable[indexPath.section]?[indexPath.row])! as DependencyTable
            
            self.delegate.managedObjectContext.deleteObject(object.dependencyObject!)
            
            
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
