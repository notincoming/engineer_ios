//
//  PropertyAdditionViewController.swift
//  Engineer_iOS
//
//  Created by Macstorm on 17.10.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit
import ReactiveCocoa

class PropertyAdditionViewController: UIViewController {

    @IBOutlet weak var baseValueTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    var completionHandler: ((String, Int) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.racObserver()
        // Do any additional setup after loading the view.
    }

    func racObserver(){
        
        let baseValueSignal = baseValueTextField.rac_textSignal().map{ (text: AnyObject!) -> AnyObject! in
            
            let textString = text as! String
            
            return ((textString as NSString).length >= 0 && Int(textString) != nil)
            
            
        }
        
        let nameSignal = nameTextField.rac_textSignal().map{ (text: AnyObject!) -> AnyObject! in
            
            let textString = text as! String
            
            return (textString as NSString).length >= 0
            
        }
        
        let validationSignal = RACSignal.combineLatest([baseValueSignal, nameSignal]).map{
            
            let tuple = $0 as! RACTuple
            let baseValue = tuple.first as! Bool
            let name = tuple.second as! Bool
            
            return baseValue && name
            
        }
        
        validationSignal.subscribeNext { result in
            
            let boolValue = result as! Bool
            
            if boolValue{
              
                self.saveButton.enabled = true
            
            }else{
                
                self.saveButton.enabled = false
            }
        }
        
        self.saveButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside).subscribeNext { _ in
            
            self.completionHandler!(self.nameTextField.text!, Int(self.baseValueTextField.text!)!)
            self.dismissViewControllerAnimated(true, completion: nil)
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
