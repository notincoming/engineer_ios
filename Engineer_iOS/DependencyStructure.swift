//
//  DependencyStructure.swift
//  Engineer_iOS
//
//  Created by Macstorm on 14.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import Foundation

let globalKey = "origin"

class DependencyStructure{
    
    var key: String?
    var container: [ String: [DependencyStructure] ]? = [ : ]
    
    init(key: String?, container: [ String: [DependencyStructure]]?){
        
        if key != nil{
            
            self.key = key
        }
        
        if container != nil{
            
            self.container = container
        }
    }
}


class GlobalDependencyStructreContainer{
    
    static let shared = GlobalDependencyStructreContainer()
    
    
    var dependencyStructure = DependencyStructure(key: globalKey, container: nil)
    
    private init(){}
    
}