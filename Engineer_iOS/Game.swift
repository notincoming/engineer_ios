//
//  Game.swift
//  Engineer_iOS
//
//  Created by Macstorm on 05.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import Foundation
import CoreData

public class Game{
    
    class func searchForGames(user: User!) -> [Game]? {
        
        let fetchRequest = NSFetchRequest(entityName: "Game")
        let predicate = NSPredicate(format: "user == %@", user.userObject!)
        let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: false)
        
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do{
            let results = try user.userObject?.managedObjectContext?.executeFetchRequest(fetchRequest)
            
            var temporaryTable = [Game]()
            
            for object in results!{
                
               // temporaryTable.append(AdventurerCard(name: object.valueForKey("name") as! String, createdAt: object.valueForKey("createdAt") as! NSDate))
                
                // MARK: TODO
                
            }
            
            return temporaryTable
            
            
        }catch{
            
            let error = error as NSError
            
            print("Error at AdventurerCard -> searchForAdventurerCards")
            print(error.userInfo)
            return nil
        }
    }
    
}