//
//  MenuViewController.swift
//  Engineer_iOS
//
//  Created by Macstorm on 13.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit
import CoreData
import ReactiveCocoa

class MenuViewController: UIViewController{
    
    @IBOutlet weak var dependencyTablesButton: UIButton!

    @IBOutlet weak var gamesButton: UIButton!
    
    
    @IBOutlet weak var adventurerCardsButton: UIButton!
    
    let delegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var fetchedResultsController: NSFetchedResultsController?
    
    var user: User!
    //var dependencyTables: [DependencyTable]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.racObserver()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.getUserData()
        
    }
    
    func getUserData(){
        
        self.user.dependencyTables = DependencyTable.searchForTablesWithNoReference(self.user)
        self.user.adventurerCards = AdventurerCard.searchForAdventurerCards(self.user)
       // self.user.games = Game.searchForGames(self.user)
        
    }
    func racObserver() -> Void{
        
        let dependencyTablesSignal: RACSignal = self.dependencyTablesButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside)
        
        dependencyTablesSignal.subscribeNext({
            
            _ in
                
           self.fetchedResultsController = {
            
            
                    let fetchRequest = NSFetchRequest(entityName: "DependencyTable")
                    
                    let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: false)
                    fetchRequest.sortDescriptors = [sortDescriptor]
            
                    let predicate = NSPredicate(format: "user == %@ && father == NULL", self.user.userObject!)
            
                    fetchRequest.predicate = predicate

                    let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.delegate.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)

                    fetchedResultsController.delegate = nil
                    
                    return fetchedResultsController
            
                }()
            
            self.performSegueWithIdentifier("DependencyTablesSegue", sender: self)
            
        })
        
        let adventurerCardsSignal: RACSignal = self.adventurerCardsButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside)
        
        adventurerCardsSignal.subscribeNext({
        
            _ in
            
            self.fetchedResultsController = {
                
                
                let fetchRequest = NSFetchRequest(entityName: "AdventurerCard")
                
                let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: false)
                fetchRequest.sortDescriptors = [sortDescriptor]
                
                let predicate = NSPredicate(format: "user == %@", self.user.userObject!)
                
                fetchRequest.predicate = predicate
                
                let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.delegate.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
                
                fetchedResultsController.delegate = nil
                
                return fetchedResultsController
                
            }()
            
            self.performSegueWithIdentifier("AdventurerCardsSegue", sender: self)
        })
        
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        switch segue.identifier!{
            
            case "DependencyTablesSegue":
                
                //let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
               // print(documentsPath)
                
                let viewController = segue.destinationViewController as! DependencyTableViewController
            
                do{
                    guard self.fetchedResultsController != nil else{
                        return
                    }
                    
                   try self.fetchedResultsController?.performFetch()
                    

                }catch{
                    
                    let error = error as NSError
                    print(error.userInfo)
                    print("Error while fetching at MenuViewController -> prepareForSegue")
            }
            
            viewController.fetchedResultsController = self.fetchedResultsController
            viewController.user = self.user
            viewController.isPrimaryObject = false
            
            case "AdventurerCardsSegue":
            
                let viewController = segue.destinationViewController as! AdventurerCardTableViewController
                
                do{
                    guard self.fetchedResultsController != nil else{
                        return
                    }
                    
                    try self.fetchedResultsController?.performFetch()
                    
                    
                }catch{
                    
                    let error = error as NSError
                    print(error.userInfo)
                    print("Error while fetching at MenuViewController -> prepareForSegue")
                }

                
                viewController.fetchedResultsController = self.fetchedResultsController
                viewController.user = self.user
        
            case "NetworkStoryBoardSegue":
            
                let navigationController = segue.destinationViewController as! UINavigationController
            
                let viewController = navigationController.viewControllers.first as! NetworkAuthenticationViewController
                
                viewController.user = self.user
            
            
            default:
                return
        }
    }
    
    

}
