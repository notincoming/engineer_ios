//
//  AdventurerCardCollectionViewController.swift
//  Engineer_iOS
//
//  Created by Macstorm on 19.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit
import CoreData
import ReactiveCocoa

private let reuseIdentifier = "reuseIdentifier"

class AdventurerCardCollectionViewController: UICollectionViewController, NSFetchedResultsControllerDelegate {

    
    
    let itemsPerRowForSection0 = 3
    let itemsPerRowForSection1 = 2
    let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    var wasCreated: Bool!
    
    struct NameAndAvailabilities {
        
        var name: String!
        var availability: Int!
        
    }
    
    var user: User!
    var adventurerCard: AdventurerCard!
    let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var dependencyNamesWithAvaialbilities: [NameAndAvailabilities]?
    var fetchedResultsController: NSFetchedResultsController?
    var blockOperation: NSBlockOperation?
    var shouldReloadCollectionView: Bool?
    var storedIndexPath: NSIndexPath!
    var tableOfDependencies: [Dependency]?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
       // self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
       // self.collectionView!.registerClass(AdventurerCardCollectionViewPropertyCell.self, forCellWithReuseIdentifier: "propertyCell")
     //   self.collectionView!.registerClass(AdventurerCardCollectionViewDependencyCell.self, forCellWithReuseIdentifier: "dependencyCell")
        
        self.title = self.adventurerCard.name
        self.collectionView?.allowsMultipleSelection = false
        
        
    }

    func recurenceFetch(referenceObject: NSManagedObject!, propertyObject: NSManagedObject!){
        
        let fetchRequest = NSFetchRequest(entityName: "DependencyTable")
        let predicate = NSPredicate(format: "user == %@ && father == %@", self.user.userObject!, referenceObject)
        let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = predicate
        
        do{
            
            let results = try self.delegate.managedObjectContext.executeFetchRequest(fetchRequest)
            
            if results.count != 0{
                
                for object in results{
                    
                    self.tableOfDependencies?.append(Dependency(name: object.valueForKey("name") as! String, dependencyTableObject: object as! NSManagedObject, equationType: "addition", propertyObject: propertyObject, value: 0, isAvailable: false, createdAt: NSDate(), dependencyObject: nil))
                    
                    
                    self.recurenceFetch(object as! NSManagedObject, propertyObject: propertyObject)
                
                }
                
                
                
            }else{
                
                return
            }
            
        }catch{
            
            let error = error as NSError
            print("Error at AdventurereCollectionCardController -> recurenceFetch with error \(error.localizedDescription)")
        }
        
        
    }
    
    func dependencyFetch(propertyObject: NSManagedObject!) -> Bool{
        
        let fetchRequest = NSFetchRequest(entityName: "Dependency")
        let predicate = NSPredicate(format: "property == %@", propertyObject)
        
        let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: true)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = predicate
        
        
        do{
            
            let results = try self.delegate.managedObjectContext.executeFetchRequest(fetchRequest)
            
            if results.count != 0{
                
                for object in results{
                    
                    
                    self.tableOfDependencies?.append(Dependency(name: object.valueForKey("name") as! String, dependencyTableObject: object.valueForKey("dependencyTable") as! NSManagedObject, equationType: object.valueForKey("equationType") as! String, propertyObject: object.valueForKey("property") as! NSManagedObject, value: object.valueForKey("value") as! Int, isAvailable: object.valueForKey("isAvailable") as! Bool, createdAt: object.valueForKey("createdAt") as! NSDate, dependencyObject: object as? NSManagedObject))
                    
                }
                
                return true
                
            }else{
                
                return false
            }
        }catch{
            
            let error = error as NSError
            print("Error at AdventurerCollectionCardController -> dependencyFetch with error \(error.localizedDescription)")
        }
        
        return false
        
    }
    func racObserver(cellReference: AdventurerCardCollectionViewPropertyCell){
        
        cellReference.dependenciesButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside).subscribeNext {
            _ in
            
            self.storedIndexPath = self.collectionView?.indexPathForCell(cellReference)
            
            self.tableOfDependencies = [Dependency]()
            
            if !self.dependencyFetch(self.adventurerCard.properties![self.storedIndexPath.row].propertyObject){
                
                self.recurenceFetch(self.adventurerCard.dependencyTable?.dependencyObject!, propertyObject: self.adventurerCard.properties![self.storedIndexPath.row].propertyObject)
                
                self.wasCreated = false
                
            }else{
                
                self.wasCreated = true
                
            }
            
            self.performSegueWithIdentifier("DependenciesOfPropertySegue", sender: self)
            
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource
    
    override func viewWillAppear(animated: Bool) {
        
        self.setTheData()
    }
    
    func setTheData(){
        
        guard self.fetchedResultsController != nil else{
            
            return
        }
        
        self.fetchedResultsController?.delegate = self
        
        var temporaryTable = [Property]()
        
        for section in (self.fetchedResultsController?.sections)! {
            
            for object in section.objects!{
                
                let property = Property(name: object.valueForKey("name") as! String, baseValue: object.valueForKey("baseValue") as! Int, propertyObject: object as! NSManagedObject)
                
                temporaryTable.append(property)
                
                
            }
            
        }
        
        self.adventurerCard.properties = temporaryTable
        
        
    }
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        guard self.adventurerCard.dependencyTable != nil else{
            return 1
        }
        return 2
        
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
        guard self.adventurerCard.dependencyTable != nil else{
            
            guard self.adventurerCard.properties != nil else{
                
                return 0
            }
            
            return (self.adventurerCard.properties?.count)!
        }
        
        if section == 0 {
            
            // MARK: To do !
            
           //return 4
            let fetchRequest = NSFetchRequest(entityName: "DependencyTable")
            let predicate = NSPredicate(format: "father == %@ && user == %@", (self.adventurerCard.dependencyTable?.dependencyObject!)!, self.user.userObject!)
            
            fetchRequest.predicate = predicate
            
            do{
                
                let results = try delegate.managedObjectContext.executeFetchRequest(fetchRequest)
                
                guard results.count != 0 else{
                    
                    return 0
                }
                
                
                self.dependencyNamesWithAvaialbilities = [NameAndAvailabilities]()
                
                for object in results{
                    
                    
                    self.dependencyNamesWithAvaialbilities?.append(NameAndAvailabilities(name: object.valueForKey("name") as! String, availability: object.valueForKey("availability") as! Int))
                    
                    
                }
                
                
                return results.count
                
            }catch{
                
                let error = error as NSError
                print("Error at AdventurerColletionViewController -> collectionView numberOfItemsInSection")
                print(error.description)
                
                return 0
            }
            
            
        }else{
            
           /* guard self.adventurerCard.properties != nil else{
                
                return 0
            }
            
            return (self.adventurerCard.properties?.count)! */
            
            guard self.fetchedResultsController != nil else{
                
                return 0
            }
            
            
            return (self.fetchedResultsController?.sections?.first?.numberOfObjects)!
            
            
        }
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
       // let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! UICollectionViewCell

       // var cell: UICollectionViewCell!
        
        //var cell: UICollectionViewCell!
        
        if indexPath.section == 0{
            
          let  cell = collectionView.dequeueReusableCellWithReuseIdentifier("dependencyCell", forIndexPath: indexPath) as! AdventurerCardCollectionViewDependencyCell

            print(self.dependencyNamesWithAvaialbilities![indexPath.row])
             cell.label.text = "\(self.dependencyNamesWithAvaialbilities![indexPath.row].availability) x " +  self.dependencyNamesWithAvaialbilities![indexPath.row].name
          
            //cell.label.text = "JAN"
           // cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
            
            //cell.frame.size.height = 100
            
          return cell
            
        }else{
            
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("propertyCell", forIndexPath: indexPath) as! AdventurerCardCollectionViewPropertyCell
            
            cell.nameLabel.text = self.adventurerCard.properties![indexPath.row].name
            cell.baseValueLabel.text = "\(self.adventurerCard.properties![indexPath.row].baseValue)"
           // cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingExpandedSize)
            
           // cell.frame.size.height = 200
            //cell.nameLabel.font = UIFont(descriptor: cell.nameLabel.font.fontDescriptor(), size: 9.0)
            
            cell.nameDescription.font = UIFont(descriptor: cell.nameDescription.font.fontDescriptor(), size: cell.baseValueDescription.font.pointSize)
            
            self.racObserver(cell)
            
            return cell
        }
        
        
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        
            self.blockOperation = NSBlockOperation()
        
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        
    
        self.collectionView?.performBatchUpdates({
            
            self.blockOperation?.start()
            
            }, completion: nil)
    
        
    }
    
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        
        switch (type) {
        case .Insert:
           
            self.blockOperation?.addExecutionBlock({ 
                
                let iPath = NSIndexPath(forRow: 0, inSection: 1)
                
                self.collectionView?.insertItemsAtIndexPaths([iPath])
                self.setTheData()
                
                
            })
            
            
            
        case .Delete:
            if let indexPath = indexPath {
               // tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            }
            break;
            
        case .Update:
            if let indexPath = indexPath {
                
                // update stuff
            }
            break;
        case .Move:
            if let indexPath = indexPath {
                //tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            }
            
            if let newIndexPath = newIndexPath {
               // tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Fade)
            }
            break;
        }
        
    }
    
    
    func completionHandler(viewController: PropertyAdditionViewController!){
        
        
        viewController.completionHandler = {
         
            [unowned self]
            
            (name, baseValue) in
            
            let entityDescription = NSEntityDescription.entityForName("Property", inManagedObjectContext: self.delegate.managedObjectContext)
            
            let propertyRecord = NSManagedObject(entity: entityDescription!, insertIntoManagedObjectContext: self.delegate.managedObjectContext)
            
            
            propertyRecord.setValue(name, forKey: "name")
            propertyRecord.setValue(NSDate(), forKey: "createdAt")
            propertyRecord.setValue(baseValue, forKey: "baseValue")
            
            
            propertyRecord.setValue(self.adventurerCard.adventurerObject!, forKey: "adventurerCard")
            
            
            do{
                
                
                try self.delegate.managedObjectContext.save()
                
                
            }catch{
                
                let error = error as NSError
                print(error.userInfo)
                print("Error during save at AdventurerCollectionViewController -> completionHandler")
            }
        
      }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        switch segue.identifier!{
            
            case "PropertyAdditionSegue":
           
                let viewController = segue.destinationViewController as! PropertyAdditionViewController
                self.completionHandler(viewController)
            
            
            case "DependenciesOfPropertySegue":
            
                let navigationController = segue.destinationViewController as! UINavigationController
                let tableViewController = navigationController.viewControllers.first as! DependenciesOfPropertyTableViewController
            
                tableViewController.dependenciesTable = self.tableOfDependencies
                tableViewController.wasCreated = self.wasCreated
            
             /*   tableViewController.property = self.adventurerCard.properties![self.storedIndexPath.row]
                tableViewController.dependencyObject = self.adventurerCard.dependencyTable?.dependencyObject!
                tableViewController.userObject = self.user.userObject! */
            
            
            default:
            break
        }
    }
    


    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
