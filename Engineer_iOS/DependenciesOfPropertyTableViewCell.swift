//
//  DependenciesOfPropertyTableViewCell.swift
//  Engineer_iOS
//
//  Created by Macstorm on 18.10.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit
import ReactiveCocoa

class DependenciesOfPropertyTableViewCell: UITableViewCell ,UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    let equationPosibilities = ["addition", "subtraction", "multiplication", "division"]
    
    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var equationTypeSelector: UIPickerView!
    @IBOutlet weak var equationTypeTextField: UITextField!
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var availabilitySwitch: UISwitch!
    
    var pickerObserver: MutableProperty<Bool>!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.equationTypeSelector.delegate = self
        self.equationTypeSelector.dataSource = self
        self.equationTypeTextField.delegate = self
        pickerObserver = MutableProperty<Bool>(false)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        // Configure the view for the selected state
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.equationPosibilities.count
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return self.equationPosibilities[row]
        
    }
    
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        self.equationTypeTextField.text = self.equationPosibilities[row]
        // self.dropDownList.hidden = true
        if self.pickerObserver.value{
            
            self.pickerObserver.value = false
            
        }else{
            
            self.pickerObserver.value = true
        }
    }
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        self.equationTypeTextField.endEditing(true)
        self.equationTypeSelector.hidden = false
    }

}
