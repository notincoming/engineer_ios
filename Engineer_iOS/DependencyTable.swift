//
//  DependencyTable.swift
//  Engineer_iOS
//
//  Created by Macstorm on 13.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit
import CoreData

class DependencyTable: NSObject {

    var name: String!
    var createdAt: NSDate!
    var availability: Int?
    var dependencyObject: NSManagedObject?
    var isUsed: Bool?
    
    init(name: String!, createdAt: NSDate!, availability: Int?, managedObject: NSManagedObject?, isUsed: Bool?){
        
        self.name = name
        self.createdAt = createdAt
        
        if availability != nil{
            
            self.availability = availability
        }
        
        if isUsed != nil{
            
            self.isUsed = isUsed
        }
        
        guard managedObject != nil else{
            return
        }
   
        self.dependencyObject = managedObject
    }
    
    
   class func searchForTablesWithNoReference(user: User!) -> [DependencyTable]? {
        
        
        
        let fetchRequest = NSFetchRequest(entityName: "DependencyTable")
        
        let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let predicate = NSPredicate(format: "user == %@ && father == NULL", user.userObject!)
        
        fetchRequest.predicate = predicate
        
        do{
            
            let results = try user.userObject!.managedObjectContext?.executeFetchRequest(fetchRequest)
            
            var temporaryTable = [DependencyTable]()
            
            for object in results!{
                
                temporaryTable.append(DependencyTable(name: object.valueForKey("name") as! String, createdAt: object.valueForKey("createdAt") as! NSDate, availability: nil, managedObject: object as? NSManagedObject, isUsed: object.valueForKey("isUsed") as? Bool))
                
                
            }
            
            return temporaryTable
            
        }catch{
            
            let error =  error as NSError
            
            print("Error at DependencyTable -> searchForTablesWithNoReference")
            print(error.description)
            
            return nil
        }
    }
}
