//
//  AdventurerCardTableViewController.swift
//  Engineer_iOS
//
//  Created by Macstorm on 13.09.2016.
//  Copyright © 2016 edu.self.Engineer. All rights reserved.
//

import UIKit
import CoreData

class AdventurerCardTableViewController: UITableViewController, NSFetchedResultsControllerDelegate{
    
    let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var user: User!
    var fetchedResultsController: NSFetchedResultsController?
    var adventurerCards = [Int: [AdventurerCard]]()
  
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.setTheData()
    }
    


    func setTheData() -> Void{
        
        guard self.fetchedResultsController != nil else{
            
            return
        }
        
        self.fetchedResultsController?.delegate = self
        
        var iterator = 0
        
        for section in (self.fetchedResultsController?.sections)! {
            
            var temporaryTable = [AdventurerCard]()
            
            for object in section.objects!{
                
                let adventurerCard = AdventurerCard(name: object.valueForKey("name") as! String, createdAt: object.valueForKey("createdAt") as! NSDate)
                
                let dependencyObject = object.valueForKey("dependencyTable") as? NSManagedObject
                
                if dependencyObject != nil{
                    
                    let dependencyTable = DependencyTable(name: dependencyObject!.valueForKey("name") as! String, createdAt: dependencyObject!.valueForKey("createdAt") as! NSDate, availability: nil, managedObject: dependencyObject, isUsed: dependencyObject!.valueForKey("isUsed") as? Bool)
                     adventurerCard.dependencyTable = dependencyTable
                }
               
                
                adventurerCard.adventurerObject = object as? NSManagedObject
                
               // temporaryTable.append(AdventurerCard(name: object.valueForKey("name") as! String, createdAt: object.valueForKey("createdAt") as! NSDate))
    
               
                temporaryTable.append(adventurerCard)
                
            }
            
            self.adventurerCards[iterator] = temporaryTable
            iterator += 1
        }
        
        self.title = "Adventurer Cards"
        
    }
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        guard self.fetchedResultsController != nil else{
            
            return 0
        }
        
        return (self.fetchedResultsController?.sections?.count)!
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        guard self.fetchedResultsController != nil else{
            
            return 0
        }
        
        return (self.fetchedResultsController?.sections![section].numberOfObjects)!
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! AdventurerCardTableViewCell
        
        cell.customLabel.text = self.adventurerCards[indexPath.section]![indexPath.row].name as String
        
        return cell
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        
        tableView.endUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        
        switch (type) {
        case .Insert:
            
            if let indexPath = newIndexPath {
                
               let element = AdventurerCard(name: anObject.valueForKey("name") as! String, createdAt: anObject.valueForKey("createdAt") as! NSDate)
                
                let dependencyObject = anObject.valueForKey("dependencyTable") as? NSManagedObject
                
                if dependencyObject != nil {
                    
                    let dependencyTable = DependencyTable(name: dependencyObject!.valueForKey("name") as! String, createdAt: dependencyObject!.valueForKey("createdAt") as! NSDate, availability: nil, managedObject: dependencyObject, isUsed: dependencyObject!.valueForKey("isUsed") as? Bool)
                    
                    element.dependencyTable = dependencyTable
                }
                
                element.adventurerObject = anObject as? NSManagedObject

                self.adventurerCards[indexPath.section]?.insert(element, atIndex: 0)
                
                
                tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            }
            break;
        case .Delete:
            if let indexPath = indexPath {
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            }
            break;
            
        case .Update:
            if let indexPath = indexPath {
                
                // update stuff
            }
            break;
        case .Move:
            if let indexPath = indexPath {
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            }
            
            if let newIndexPath = newIndexPath {
                tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Fade)
            }
            break;
        }
        
    }
    

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        guard segue.identifier != nil else{
            return
        }
        switch segue.identifier!{
            
            case "AdventurerCardAdditionSegue":
                let viewController = segue.destinationViewController as! AdventurerCardAdditionViewController
               // viewController.dependencyTable = self.dependencyTables
                
                self.addCompletion(viewController)
                viewController.user = self.user
            
            case "AdventurerCardCollectionSegue":
                
                let viewController = segue.destinationViewController as! AdventurerCardCollectionViewController
                viewController.user = self.user
                
                let indexPath = self.tableView.indexPathForSelectedRow!
                
                let fetchedResultsControllerToPass: NSFetchedResultsController = {
                    
                    let fetchRequest = NSFetchRequest(entityName: "Property")
                    
                    let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: false)
                    fetchRequest.sortDescriptors = [sortDescriptor]
                    
                    let predicate = NSPredicate(format: "adventurerCard == %@", self.adventurerCards[indexPath.section]![indexPath.row].adventurerObject!)
                    
                    
                    fetchRequest.predicate = predicate
                    
                    let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.delegate.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
                    
                    fetchedResultsController.delegate = nil
                    
                    return fetchedResultsController
                    
                }()
                
                do{
                    
                    try fetchedResultsControllerToPass.performFetch()
                    
                }catch{
                    
                    let error = error as NSError
                    print(error.userInfo)
                    print("Error while reinitialized fetch at AdventurerCardController -> didSelectRow -> prepeareForSegue")
                }
                
                viewController.fetchedResultsController = fetchedResultsControllerToPass
                viewController.adventurerCard = self.adventurerCards[indexPath.section]![indexPath.row]
            
        default:
            return
        }
    }
    
    func addCompletion(viewController: AdventurerCardAdditionViewController){
        
        viewController.completion = {
        
            [unowned self]
            
            (name, dependencyTable) in
            
            let entityDescription = NSEntityDescription.entityForName("AdventurerCard", inManagedObjectContext: self.delegate.managedObjectContext)
            let cardRecord = NSManagedObject(entity:  entityDescription!, insertIntoManagedObjectContext: self.delegate.managedObjectContext)
            
            cardRecord.setValue(name, forKey: "name")
            cardRecord.setValue(NSDate(), forKey: "createdAt")
            
            if dependencyTable != nil{
                
                    
                dependencyTable?.dependencyObject!.setValue(true, forKey: "isUsed")
                    
                cardRecord.setValue(dependencyTable?.dependencyObject!, forKey: "dependencyTable")

            }
            
            cardRecord.setValue(self.user.userObject!, forKey: "user")
            
            do{
                try cardRecord.managedObjectContext!.save()
                
            }catch{
                
                let error = error as NSError
                print(error.userInfo)
                //  print("Error during save at DependencyTableViewController -> addDependencyTable")
            }
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        
        return true
    }
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
         
            let object = (self.adventurerCards[indexPath.section]?[indexPath.row])! as AdventurerCard
            
            self.delegate.managedObjectContext.deleteObject(object.adventurerObject!)
            
        }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
